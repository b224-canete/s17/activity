console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function getUserProfile() {
		let fullName = prompt("What is your name?");
		let ageUser = prompt("How old are you?");
		let addressUser = prompt("Where do you live?")
	
		alert("Thank you for your input!");
		console.log("Hello, " + fullName);
		console.log("You are "+ ageUser + " years old.");
		console.log("You live in " + addressUser);
		};

	getUserProfile();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function showFaveArtist() {
		let firstArtist = "Bruno Mars";
		let secondArtist = "Ed Sheeran";
		let thirdArtist = "Charlie Puth";
		let fourthArtist = "Adam Lavine";
		let fifthArtist = "Zayn";

		console.log("1. " + firstArtist);
		console.log("2. " + secondArtist);
		console.log("3. " + thirdArtist);
		console.log("4. " + fourthArtist);
		console.log("5. " + fifthArtist);
	}

	showFaveArtist();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function showFaveMovies() {
		let firstMovie = "Lord of the Rings, III";
		let secondMovie = "Lord of the Rings, I";
		let thirdMovie = "Lord of the Rings, II";
		let fourthMovie = "The Hobbit, III";
		let fifthMovie = "The Hobbit, II";
		let firstRating = "93%";
		let secondRating = "91%";
		let thirdRating = "95%";
		let fourthRating = "59%";
		let fifthRating = "74%";

		console.log("1. " + firstMovie);
		console.log("Rotten Tomatoes Rating: " + firstRating);
		console.log("2. " + secondMovie);
		console.log("Rotten Tomatoes Rating: " + secondRating);
		console.log("3. " + thirdMovie);
		console.log("Rotten Tomatoes Rating: " + thirdRating);
		console.log("4. " + fourthMovie);
		console.log("Rotten Tomatoes Rating: " + fourthRating);
		console.log("5. " + fifthMovie);
		console.log("Rotten Tomatoes Rating: " + fifthRating);
	}

	showFaveMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);